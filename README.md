This repository contains mini-projects for learning Keras - neural network library.  

sine.py:  
This script tries to approximate sine function with a neural network and plots results:  
plot of function sin(x) is green, neural net approximation is blue.  

points.py:  
Interactive points classifier.  
Right/left - click to put points (training data) on the image.  
Press space to let the neural net classify all image points based on the chosen points.  

Convolutional0:  
Recognizing digits based on MNIST database.   
Possible command line arguments:   
- train [path to file where trained model will be saved]   
- predict [path to file with trained model] [path to file with image]   
You can pass any image to predict, however for proper results:   
there should be one white digit on black background.   
