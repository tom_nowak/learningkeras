#!/usr/bin/python

# This script tries to approximate sine function with a neural network and plots results:
# plot of function sin(x) is green, neural net approximation is blue.

import numpy as np
from keras.models import Sequential
from keras.layers import Dense
import matplotlib.pyplot as plt


def tested_function(array):
    return np.sin(array*np.pi)


print("creating neural net...")
model = Sequential()
model.add(Dense(128, input_dim=1, activation='sigmoid'))
model.add(Dense(1, activation='linear'))
model.compile(optimizer='rmsprop', loss='mse')

print("training neural net...")
train_x = np.random.uniform(-1.0, 1.0, 2 ** 19)
train_y = tested_function(train_x)
model.fit(train_x, train_y, epochs=10, batch_size=64, verbose=0)
del train_x
del train_y

print("running prediction...")
x = np.arange(-1.0, 1.0, 0.001)
y = model.predict(x)
expected_y = tested_function(x)
plt.plot(x, y, x, expected_y, 'g')
plt.show()
print("finished")
