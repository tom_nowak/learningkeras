#!/usr/bin/python

# Recognizing digits based on MNIST database.
# Possible command line arguments:
# - train [path to file where trained model will be saved]
# - predict [path to file with trained model] [path to file with image]
# You can pass any image to predict, however for proper results:
# there should be one white digit on black background.

import keras
from keras.datasets import mnist
from keras.layers import Dense, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.models import Sequential, load_model
import numpy as np
from PIL import Image
import sys


class Model:
    BATCH_SIZE = 128
    NUM_CLASSES = 10
    EPOCHS = 10
    INPUT_SHAPE = (28, 28, 1)

    def __init__(self, path=None):
        if path is None:
            self._model = Sequential()
            self._model.add(Conv2D(32, kernel_size=(5, 5), strides=(1, 1),
                                   activation='relu', input_shape=Model.INPUT_SHAPE))
            self._model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
            self._model.add(Conv2D(64, (5, 5), activation='relu'))
            self._model.add(MaxPooling2D(pool_size=(2, 2)))
            self._model.add(Flatten())
            self._model.add(Dense(1000, activation='relu'))
            self._model.add(Dense(Model.NUM_CLASSES, activation='softmax'))
            self._model.compile(loss=keras.losses.categorical_crossentropy,
                                optimizer=keras.optimizers.Adam(), metrics=['accuracy'])
        else:
            self._model = load_model(path)

    def train(self):
        (x_train, y_train), (x_test, y_test) = mnist.load_data()
        x_train = x_train.reshape((x_train.shape[0],) + Model.INPUT_SHAPE)
        x_test = x_test.reshape((x_test.shape[0],) + Model.INPUT_SHAPE)
        x_train = x_train.astype('float32') / 255.0
        x_test = x_test.astype('float32') / 255.0
        y_train = keras.utils.to_categorical(y_train, Model.NUM_CLASSES)
        y_test = keras.utils.to_categorical(y_test, Model.NUM_CLASSES)
        self._model.fit(x_train, y_train, batch_size=Model.BATCH_SIZE, epochs=Model.EPOCHS,
                        verbose=1, validation_data=(x_test, y_test))
        return self._model.evaluate(x_test, y_test, verbose=0)

    def save(self, path):
        self._model.save(filepath=path)

    def predict(self, image):
        return self._model.predict(image, batch_size=Model.BATCH_SIZE)


def print_help(program_name):
    print("usage:\n", program_name, " train [path to save model]\n",
          program_name, " predict [path to read model] [path to image] ", sep="")
    sys.exit(-1)


def read_image(path):
    im = Image.open(path).convert('L')
    im = im.resize((Model.INPUT_SHAPE[0], Model.INPUT_SHAPE[1]), Image.ANTIALIAS)
    arr = np.array(im.getdata()).reshape((1,) + Model.INPUT_SHAPE)
    return arr.astype('float32') / 255.0


def train(model_output_file):
    m = Model()
    score = m.train()
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])
    m.save(model_output_file)


def predict(model_input_file, image_input_file):
    m = Model(model_input_file)
    img = read_image(image_input_file)
    output = m.predict(img)
    formatted_output = output[0, :] * 100
    numbers = formatted_output.argsort()
    for i in reversed(range(Model.NUM_CLASSES)):
        print("{}: {:.2f}%".format(numbers[i], formatted_output[numbers[i]]))
        if formatted_output[numbers[i]] < 0.01:
            return


if __name__ == "__main__":
    if len(sys.argv) == 3:
        if sys.argv[1] != "train":
            print_help(sys.argv[0])
        train(sys.argv[2])
    elif len(sys.argv) == 4:
        if sys.argv[1] != "predict":
            print_help(sys.argv[0])
        predict(sys.argv[2], sys.argv[3])
    else:
        print_help(sys.argv[0])
