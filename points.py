#!/usr/bin/python

# Right/left - click to put points (training data) on the image.
# Press space to let the neural net classify all image points based on the chosen points.

from tkinter import Tk, Canvas, PhotoImage, mainloop, ALL
import numpy as np
from keras.models import Sequential
from keras.layers import Dense
from keras.utils import to_categorical


def normalize(value, max_value):
    return value/max_value


def denormalize(value, max_value):
    return value*max_value


def saturate(value, min_value=0, max_value=255):
    if value < min_value:
        return min_value
    if value > max_value:
        return max_value
    return value


class NeuralNet:
    def __init__(self):
        self.epoch_step = 500
        self._last_epoch = 0
        self._train_input = np.empty(0)
        self._train_output = np.empty(0)
        self._model = Sequential()
        self._model.add(Dense(64, input_dim=2, activation='relu'))
        self._model.add(Dense(64, activation='relu'))
        self._model.add(Dense(2, activation='softmax'))
        self._model.compile(optimizer='rmsprop', loss='binary_crossentropy')
        self._initial_weights = self._model.get_weights()

    def retrain(self, points0, points1):
        self._model.set_weights(self._initial_weights)
        if not points0 and not points1:
            return
        self._train_input = np.array(points0 + points1)
        outputs = np.empty(self._train_input.shape[0])
        outputs[:len(points0)] = 0
        outputs[len(points0):] = 1
        self._train_output = to_categorical(outputs, num_classes=2)
        self._last_epoch = 0
        print("retrain for input:\npoints0:\n", self._train_input[:len(points0)],
              "\npoints1:\n", self._train_input[len(points0):], "\n", sep='')
        self.train()

    def train(self):
        epochs = self.epoch_step + self._last_epoch
        print("training epochs from", self._last_epoch, "to", epochs, end="", flush=True)
        self._model.fit(self._train_input, self._train_output, epochs=epochs,
                        batch_size=self._train_input.shape[1], verbose=0, initial_epoch=self._last_epoch)
        self._last_epoch = epochs
        print(" - finished")

    def predict(self, data):
        return self._model.predict(data)


class Window:
    def __init__(self, neural_net):
        self.neural_net = neural_net
        self.R = 4
        self.waiting = False
        self.points_modified = True
        self.points = [[], []]
        self.window = Tk()
        self.previous_size = (0, 0)
        self.window.title("Pick points / run NN with space / quit with ESC")
        self.canvas = Canvas(self.window, bg="#c0c0c0", highlightthickness=0)
        self.canvas.pack()
        self.img = PhotoImage()
        self.bind_events()

    def bind_events(self):
        self.window.bind("<Button-1>", lambda event: self.add_point(self.points[0], event.x, event.y))
        self.window.bind("<Button-3>", lambda event: self.add_point(self.points[1], event.x, event.y))
        self.window.bind("<Escape>", lambda event: self.window.quit())
        self.window.bind("<space>", lambda event: self.redraw())
        self.window.bind("<Configure>", self.resize)

    def redraw(self):
        self.waiting = True
        self.canvas.delete(ALL)
        self.window.title("Wait - running NN...")
        w, h = self.window.winfo_width(), self.window.winfo_height()
        self.img = PhotoImage(width=w, height=h)
        self.canvas.create_image((w/2, h/2), image=self.img, state="normal")
        self.run_neural_net()
        self.draw_points()
        self.window.title("Pick points / run NN with space / quit with ESC")
        self.waiting = False
        self.points_modified = False

    def draw_points(self):
        w, h = self.window.winfo_width(), self.window.winfo_height()
        for p in self.points[0]:
            x, y = denormalize(p[0], w), denormalize(p[1], h)
            self.canvas.create_oval(x - self.R, y - self.R, x + self.R, y + self.R, fill="black", width=0)
        for p in self.points[1]:
            x, y = denormalize(p[0], w), denormalize(p[1], h)
            self.canvas.create_oval(x - self.R, y - self.R, x + self.R, y + self.R, fill="blue", width=0)

    def run_neural_net(self):
        if self.points_modified:
            self.neural_net.retrain(self.points[0], self.points[1])
        else:
            self.neural_net.train()
        w, h = self.window.winfo_width(), self.window.winfo_height()
        print("drawing new image", end="", flush=True)
        nn_input = np.empty([w, 2])
        nn_input[:, 0] = np.linspace(0.0, 1.0, num=w, endpoint=False)
        for y in range(0, h):
            nn_input[:, 1] = normalize(y, h)
            nn_output = self.neural_net.predict(nn_input)
            self.draw_nn_output(nn_output, y, w)
        print(" - finished")

    def draw_nn_output(self, nn_output, y, width):
        for x in range(0, width):
            r = saturate(int(255.0 * nn_output[x, 0]))
            g = saturate(int(255.0 * nn_output[x, 1]))
            colour = "#" + "{:02x}".format(r) + "{:02x}".format(g) + "00"
            self.img.put(colour, (x, y))

    def add_point(self, points_list, x, y):
        if self.waiting:
            return
        w, h = self.window.winfo_width(), self.window.winfo_height()
        points_list.append((normalize(x, w), normalize(y, h)))
        self.canvas.delete(ALL)
        self.draw_points()
        self.points_modified = True

    def resize(self, event):
        if self.waiting:
            return
        w, h = event.width, event.height
        if self.previous_size == (w, h):
            return
        self.previous_size = (w, h)
        self.canvas.delete(ALL)
        self.canvas.config(width=w, height=h)
        self.draw_points()


if __name__ == "__main__":
    nn = NeuralNet()
    window = Window(nn)
    mainloop()
